//
//  TKForecastCollectionViewCell.swift
//  TKWeatherApplication
//
//  Created by student on 5/3/17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit

class TKForecastCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var unitOfTimeLabel: UILabel!
    @IBOutlet weak var describeWeatherImageView: UIImageView!
    @IBOutlet weak var degreesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configurateCell(unitOfTime: String, describeWeatherImage: UIImage, degrees: String) {
        unitOfTimeLabel.text = unitOfTime
        describeWeatherImageView.image = describeWeatherImage
        degreesLabel.text = degrees
    }
}
