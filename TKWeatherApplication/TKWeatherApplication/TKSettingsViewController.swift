//
//  TKSettingsViewController.swift
//  TKWeatherApplication
//
//  Created by student on 4/26/17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit

protocol TKSettingsProtocol {
    func reloadData()
}

class TKSettings: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var degreesSegmentedControl:UISegmentedControl?
    var windSegmentedControl:UISegmentedControl?
    var pressureSegmentedControl:UISegmentedControl?
    var delegate: TKSettingsProtocol?
    var settingsStorage: TKStorageProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        configureNavigationController()
        configureDegreesSegmentedControl()
        configureWindSegmentedControl()
        configurePressureSegmentedControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getSettings()
    }
    
    func configureNavigationController() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = kSettingsScreenTitle
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(TKSettings.segmentedControlChanged))

        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    }
    
    //MARK: - ConfigureSegmentedControl
    
    func configureDegreesSegmentedControl() {
        let arrayOfSegmentItems = [kFahrenheitSymbol, kCelsius]
        degreesSegmentedControl = UISegmentedControl.init(items: arrayOfSegmentItems)
        guard let segmentedControl = degreesSegmentedControl else { fatalError() }
        segmentedControl.selectedSegmentIndex = 1;
        segmentedControl.frame = CGRect(x: 20, y: 0, width: 150, height: 35)
        segmentedControl.backgroundColor = UIColor(red: 132.0/255.0, green: 132.0/255.0, blue: 132.0/255.0, alpha: 1.0)
    }
    
    func configureWindSegmentedControl() {
        let arrayOfSegmentItems = [kMilesPerHour, kKilometersPerHour, kMetersPerSecond]
        windSegmentedControl = UISegmentedControl.init(items: arrayOfSegmentItems)
        guard let segmentedControl = windSegmentedControl else { fatalError() }
        segmentedControl.frame = CGRect(x: 20, y: 0, width: 300, height: 35)
        segmentedControl.backgroundColor = UIColor(red: 132.0/255.0, green: 132.0/255.0, blue: 132.0/255.0, alpha: 1.0)
    }
    
    func configurePressureSegmentedControl() {
        let arrayOfSegmentItems = [kMmHg, kGPa, kBar]
        pressureSegmentedControl = UISegmentedControl.init(items: arrayOfSegmentItems)
        guard let segmentedControl = pressureSegmentedControl else { fatalError() }
        segmentedControl.frame = CGRect(x: 20, y: 0, width: 300, height: 35)
        segmentedControl.backgroundColor = UIColor(red: 132.0/255.0, green: 132.0/255.0, blue: 132.0/255.0, alpha: 1.0)
    }
    
    func getSettings() {
        guard let setting = settingsStorage?.getSettings() else { fatalError() }
        
        guard let settingDegrees = setting[kSettingsDegrees] as? String else { fatalError() }
        if settingDegrees == kCelsius {
            degreesSegmentedControl?.selectedSegmentIndex = 1;
        }
        else {
            degreesSegmentedControl?.selectedSegmentIndex = 0;
        }
        
        guard let settingWind = setting[kSettingsWind] as? String else { fatalError() }
        if settingWind == kMilesPerHour {
            windSegmentedControl?.selectedSegmentIndex = 0;
        }
        if settingWind == kKilometersPerHour {
            windSegmentedControl?.selectedSegmentIndex = 1;
        }
        if settingWind == kMetersPerSecond {
            windSegmentedControl?.selectedSegmentIndex = 2;
        }
        
        guard let settingPressure = setting[kSettingsPressure] as? String else { fatalError() }
        if settingPressure == kMmHg {
            pressureSegmentedControl?.selectedSegmentIndex = 0;
        }
        if settingPressure == kGPa {
            pressureSegmentedControl?.selectedSegmentIndex = 1;
        }
        if settingPressure == kBar {
            pressureSegmentedControl?.selectedSegmentIndex = 2;
        }
    }
    
    //MARK: - CollectionView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: kTableViewCell)
        cell.textLabel!.textColor = UIColor.white
        cell.backgroundColor = UIColor.darkGray
        
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.textLabel?.text = kFirstSubmenuTitle
            }
            else {
                guard let segmentedControl = degreesSegmentedControl else {fatalError()}
                cell.addSubview(segmentedControl)
            }
        }
        else {
            if indexPath.section == 1 {
                if indexPath.row == 0 {
                    cell.textLabel?.text = kSecondSubmenuTitle
                }
                else {
                    guard let segmentedControl = windSegmentedControl else {fatalError()}
                    cell.addSubview(segmentedControl)
                }
            }
            else if indexPath.section == 2 {
                if indexPath.row == 0 {
                    cell.textLabel?.text = kThirdSubmenuTitle
                }
                else {
                    guard let segmentedControl = pressureSegmentedControl else {fatalError()}
                    cell.addSubview(segmentedControl)
                }
            }
            
        }
        return cell
    }
    
    func segmentedControlChanged() {
        var degreesValue = kCelsius
        var windValue = kKilometersPerHour
        var pressureValue = kMmHg
        
        guard let selectedIndexDegrees = degreesSegmentedControl?.selectedSegmentIndex else { fatalError() }
        if selectedIndexDegrees == 0 {
            degreesValue = kFahrenheit
        } else {
            degreesValue = kCelsius
        }
        
        guard let selectedIndexWind = windSegmentedControl?.selectedSegmentIndex else { fatalError() }
        if selectedIndexWind == 0 {
            windValue = kMilesPerHour
        }
        if selectedIndexWind == 1 {
            windValue = kKilometersPerHour
        }
        if selectedIndexWind == 2 {
            windValue = kMetersPerSecond
        }
        
        guard let settingPressure = pressureSegmentedControl?.selectedSegmentIndex else { fatalError() }
        if settingPressure == 0 {
            pressureValue = kMmHg
        }
        if settingPressure == 1 {
            pressureValue = kGPa
        }
        if settingPressure == 2 {
            pressureValue = kBar
        }
        
        settingsStorage?.saveSettings(dictionary: [kSettingsDegrees : degreesValue,
                                                   kSettingsWind : windValue,
                                                   kSettingsPressure : pressureValue])

        guard delegate != nil else {fatalError()}
        delegate?.reloadData()
    }
    

}
