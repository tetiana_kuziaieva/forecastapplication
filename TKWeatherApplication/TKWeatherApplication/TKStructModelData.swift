//
//  TKStructModelData.swift
//  TKWeatherApplication
//
//  Created by student on 4/24/17.
//  Copyright © 2017 student. All rights reserved.
//

import Foundation

struct ForecastPreview {
    let minTemperatureInCelsius: Int
    let maxTemperatureInCelsius: Int
    let currentTemperatureInCelsius: Int
    let locationName: String
    let cloudinessInPercent: Double
    let descriptionWeather: String
}
