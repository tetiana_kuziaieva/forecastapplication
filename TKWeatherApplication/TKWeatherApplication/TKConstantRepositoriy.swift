//
//  TKConstantRepositoriy.swift
//  TKWeatherApplication
//
//  Created by student on 5/3/17.
//  Copyright © 2017 student. All rights reserved.
//

import Foundation

//MARK: - ApiKey
let kDarkSkyApiKey = "f5341a9eff8c066b518bb1c7f7028b4b"
let kFlickrApiKey = "5ab51a91d0daef656bdb503a909fc040"

//MARK: - Settings
let kSettingsDegrees = "SettingDegrees"
let kCelsius = "Celsius"
let kFahrenheit = "Fahrenheit"
let kSettingsWind = "SettingWind"
let kSettingsPressure = "SettingPressure"

//MARK: - NavigationBarTitles
let kSettingsScreenTitle = "Единицы"
let kDetailsScreenTitle = "Подробнее"

//MARK: - SegmentedControlItems
let kFahrenheitSymbol = "ºF"
let kCelsiusSymbol = "ºC"
let kMilesPerHour = "миль/ч"
let kKilometersPerHour = "км/ч"
let kMetersPerSecond = "м/с"
let kMmHg = "мм рт. ст"
let kGPa = "гПа"
let kBar = "бары"

//MARK: - SubmenuTitles
let kFirstSubmenuTitle = "Температура"
let kSecondSubmenuTitle = "Скорость ветра"
let kThirdSubmenuTitle = "Давление"

//MARK: - ViewCell
let kTableViewCell = "Cell"
let kCollectionViewCell = "CollectionCell"

//MARK: -DarkSkyApi
let kCurrently = "currently"
let kSummary = "summary"
let kDaily = "daily"
let kHourly = "hourly"
let kTemperature = "temperature"
let kMaxTemperature = "apparentTemperatureMax"
let kData = "data"
let kMinTemperature = "apparentTemperatureMin"
let kHumidity = "humidity"

//MARK: -FlickrApi
let kPhotos = "photos"
let kPhoto = "photo"
let kId = "id"
let kServer = "server"
let kFarm = "farm"
let kSecret = "secret"
