//
//  ForecastRepositoriy.swift
//  TKWeatherApplication
//
//  Created by student on 4/24/17.
//  Copyright © 2017 student. All rights reserved.
//

import Foundation
import CoreLocation

protocol TKForecastRepositoriyProtocol {
    func fetchWeatherForecast(locationCoordinates: LocationParameters, setting: TKStorageProtocol, completion: @escaping (ForecastPreview?) -> Void)
}

class TKForecastRepositoriy {
     let baseApiURL = "https://api.darksky.net/forecast/f5341a9eff8c066b518bb1c7f7028b4b/"
}

extension TKForecastRepositoriy: TKForecastRepositoriyProtocol {
    
    func fetchWeatherForecast(locationCoordinates: LocationParameters, setting: TKStorageProtocol, completion: @escaping (ForecastPreview?) -> Void) {
        
        let searchUrl = setUpUrlRequestWeather(locationCoordinates: locationCoordinates, setting: setting )
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: searchUrl as URL) { (data, response, error) in
            guard let checkData = data else { fatalError() }
            let modelData = self.parseWeatherJson(data: checkData)
            completion(modelData)
        }
        
        task.resume()
    }
    
    func setUpUrlRequestWeather(locationCoordinates: LocationParameters, setting: TKStorageProtocol) -> URL {
        
        let latitude = "\(locationCoordinates.latitude)"
        let longitude = "\(locationCoordinates.longitude)"
        guard var urlComponents = URLComponents(string: baseApiURL) else { fatalError() }
        let settingsDictionary = setting.getSettings()
        var settingsItems = [URLQueryItem(name: "lang", value: "ru")]
        let coordinatesComponent = latitude + "," + longitude
        guard let degreesSettings = settingsDictionary[kSettingsDegrees] as? String else { fatalError() }
        if degreesSettings == kCelsius {
            settingsItems.append(URLQueryItem(name: "units", value: "si"))
        }
        let basePath = urlComponents.path
        let resultPath = basePath + coordinatesComponent
        urlComponents.queryItems = settingsItems
        urlComponents.path = resultPath
        guard let resultURL = urlComponents.url else { fatalError() }
        
        return resultURL
    }
    
    func parseWeatherJson(data: Data) -> ForecastPreview? {
        
        var modelData: ForecastPreview? = nil
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            guard let currently = jsonResult?[kCurrently] as? [String:Any] else {fatalError()}
            guard let description = currently[kSummary] as? String else {fatalError()}
            guard let daily = jsonResult?[kDaily] as? [String:Any] else {fatalError()}
            guard let currentTemperature = currently[kTemperature] as? Int else {fatalError()}
            guard let data = daily[kData] as? [Any] else {fatalError()}
            guard let today = data[0] as? [String:Any] else {fatalError()}
            guard let maxTemperature = today[kMaxTemperature] as? Int else {fatalError()}
            guard let minTemperature = today[kMinTemperature] as? Int else {fatalError()}
            guard let humidity = today[kHumidity] as? Double else {fatalError()}
            let locationName = "Харьков"
            
            modelData = ForecastPreview(minTemperatureInCelsius: minTemperature,
                                        maxTemperatureInCelsius: maxTemperature,
                                        currentTemperatureInCelsius: currentTemperature,
                                        locationName: locationName,
                                        cloudinessInPercent: humidity*100,
                                        descriptionWeather:description)
            
        } catch {
            print("error")
        }

        //                let originLocation = CLLocation.init(latitude: latitude, longitude: longitude)
        //                self.getPlacemark(forLocation: originLocation) { (originPlacemark, error) in
        //                    if let err = error {
        //                        print(err)
        //                    } else if let placemark = originPlacemark {
        //                        locationName = placemark.locality!
        //                    }
        //                }
        
        return modelData
    }
    
    func getPlacemark(forLocation location: CLLocation, completionHandler: @escaping (CLPlacemark?, String?) -> ()) {
        let geocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {
            placemarks, error in
            
            if let err = error {
                completionHandler(nil, err.localizedDescription)
            } else if let placemarkArray = placemarks {
                if let placemark = placemarkArray.first {
                    completionHandler(placemark, nil)
                } else {
                    completionHandler(nil, "Placemark was nil")
                }
            } else {
                completionHandler(nil, "Unknown error")
            }
        })
    }
    
    func fetchHourlyWeatherForecast(locationCoordinates: LocationParameters, setting: TKStorageProtocol, completion: @escaping (ForecastPreview?) -> Void) {
        
        let searchUrl = setUpUrlRequestWeather(locationCoordinates: locationCoordinates, setting: setting )
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: searchUrl as URL) { (data, response, error) in
            guard let checkData = data else { fatalError() }
            let modelData = self.parseWeatherJson(data: checkData)
            completion(modelData)
        }
        
        task.resume()
    }
    
    func parseJsonForHourly(data: Data) -> ForecastPreview? {
        
        var modelData: ForecastPreview? = nil
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            guard let hourly = jsonResult?[kHourly] as? [String:Any] else {fatalError()}
            guard let data = hourly[kData] as? [String:Any] else {fatalError()}
//            guard let daily = jsonResult?["daily"] as? [String:Any] else {fatalError()}
//            guard let currentTemperature = currently["temperature"] as? Int else {fatalError()}
//            guard let data = daily["data"] as? [Any] else {fatalError()}
//            guard let today = data[0] as? [String:Any] else {fatalError()}
//            guard let maxTemperature = today["apparentTemperatureMax"] as? Int else {fatalError()}
//            guard let minTemperature = today["apparentTemperatureMin"] as? Int else {fatalError()}
//            guard let humidity = today["humidity"] as? Double else {fatalError()}
//            let locationName = "Харьков"
////            
//            modelData = ForecastPreview(minTemperatureInCelsius: minTemperature,
//                                        maxTemperatureInCelsius: maxTemperature,
//                                        currentTemperatureInCelsius: currentTemperature,
//                                        locationName: locationName,
//                                        cloudinessInPercent: humidity*100,
//                                        descriptionWeather:description)
//            
        } catch {
            print("error")
        }
        
        return modelData
    }
}
