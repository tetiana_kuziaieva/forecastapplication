//
//  TKDetailsViewController.swift
//  TKWeatherApplication
//
//  Created by student on 5/3/17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit

class TKDetails: UIViewController {
    @IBOutlet weak var backgroundImageView: UIImageView!
    var backgroundImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationController()
        configureImageView(image: backgroundImage)
    }
    
    func configureImageView(image: UIImage?) {
        guard image != nil  else { fatalError() }
        self.backgroundImageView.image = image
    }
    
    func configureNavigationController() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = kDetailsScreenTitle
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    }
}
