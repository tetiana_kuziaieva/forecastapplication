//
//  ViewController.swift
//  TKWeatherApplication
//
//  Created by student on 4/24/17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit


class ViewController: UIViewController, TKSettingsProtocol {
    
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    @IBOutlet weak var minTemperatureLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var descriptionWeatherLabel: UILabel!
    @IBOutlet weak var descriptionWeatherImageView: UIImageView!
    let forecast = TKForecastRepositoriy()
    let settingsForWeather = TKStorage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadImage()
        reloadData()
    }
    
    func reloadImage() {
        let forecastCoordinate = LocationManager.getLocation()
        let backgroundManager = TKBackgroundManager()
        backgroundManager.searchPhotoInFlickr(locationCoordinates: forecastCoordinate, completion: { (image: UIImage) -> Void in
            DispatchQueue.main.async {
                self.backgroundImageView.image = image
            }
        })
    }
    
    func reloadData() {
        let forecastCoordinate = LocationManager.getLocation()
        forecast.fetchWeatherForecast(locationCoordinates: forecastCoordinate, setting: settingsForWeather, completion: { (model: ForecastPreview?) -> Void in
            guard let dataModel = model else { fatalError() }
            DispatchQueue.main.async {
                self.currentTemperatureLabel.text = "\(dataModel.currentTemperatureInCelsius)º"
                self.maxTemperatureLabel.text = "\(dataModel.maxTemperatureInCelsius)º"
                self.minTemperatureLabel.text = "\(dataModel.minTemperatureInCelsius)º"
                self.locationNameLabel.text = dataModel.locationName
                self.descriptionWeatherLabel.text = dataModel.descriptionWeather
                self.descriptionWeatherImageView.image = UIImage.init(named: dataModel.descriptionWeather.lowercased())
            }
        })
    }
    
    @IBAction func reloadDataFromButton(_ sender: UIButton) {
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? TKSettings {
            viewController.delegate = self
            viewController.settingsStorage = settingsForWeather
        }
        if let viewController = segue.destination as? TKDetails {
            //viewController.delegate = self
            //viewController.settingsStorage = settingsForWeather
            guard let image = self.backgroundImageView.image else { fatalError() }
            viewController.backgroundImage = image
        }
    }
}
