//
//  TKStorage.swift
//  TKWeatherApplication
//
//  Created by student on 5/3/17.
//  Copyright © 2017 student. All rights reserved.
//

import Foundation

protocol TKStorageProtocol {
    func saveSettings(dictionary: [String: Any])
    func getSettings() -> [String: Any]
}

class TKStorage {
}

extension TKStorage: TKStorageProtocol {
    
    func saveSettings(dictionary: [String: Any]) {
        let defaults = UserDefaults.standard
        guard let degrees = dictionary[kSettingsDegrees] else {fatalError()}
        defaults.set(degrees, forKey: kSettingsDegrees)
        
        guard let wind = dictionary[kSettingsWind] else {fatalError()}
        defaults.set(wind, forKey: kSettingsWind)
        
        guard let pressure = dictionary[kSettingsPressure] else {fatalError()}
        defaults.set(pressure, forKey: kSettingsPressure)
    }
    
    func getSettings() -> [String: Any] {
        let defaults = UserDefaults.standard
        if defaults.object(forKey: kSettingsWind) == nil {
            saveSettings(dictionary: [kSettingsDegrees: kCelsius,
                                      kSettingsWind: kKilometersPerHour,
                                      kSettingsPressure: kMmHg])
        }
        guard let degrees = defaults.object(forKey: kSettingsDegrees) else {fatalError()}
        guard let wind = defaults.object(forKey: kSettingsWind) else {fatalError()}
        guard let pressure = defaults.object(forKey: kSettingsPressure) else {fatalError()}
        let setting = [kSettingsDegrees: degrees,
                       kSettingsWind: wind,
                       kSettingsPressure: pressure]

        return setting
    }
}
