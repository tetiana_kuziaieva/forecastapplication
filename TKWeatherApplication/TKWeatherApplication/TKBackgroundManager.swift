//
//  TKBackgroundManager.swift
//  TKWeatherApplication
//
//  Created by student on 4/26/17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit

class TKBackgroundManager {
    private let baseApiURL = "https://api.flickr.com/services/rest/"
    
     func searchPhotoInFlickr(locationCoordinates: LocationParameters, completion: @escaping (UIImage) -> Void) {
        let searchURL = self.setUpUrlRequestBackground(locationCoordinates: locationCoordinates)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: searchURL as URL) { (data, response, error) in
            guard let checkData = data else { fatalError() }
            guard let urlPhoto = TKBackgroundManager.parseRequestBackground(data: checkData) else { fatalError() }
            TKBackgroundManager.loadImage(urlString: urlPhoto, completion: { (image:UIImage) in
                completion(image)
            })
        }
        task.resume()
    }

    func setUpUrlRequestBackground(locationCoordinates: LocationParameters) -> URL {
        let tags = ["sky", "sun", "night"]
        let tagsString = tags.joined(separator: ",")
        let latitude = "\(locationCoordinates.latitude)"
        let longitude = "\(locationCoordinates.longitude)"
        guard var urlComponents = URLComponents(string: baseApiURL) else { fatalError() }
        let queryItems = [ URLQueryItem(name: "method", value: "flickr.photos.search"),
                           URLQueryItem(name: "api_key", value: "5ab51a91d0daef656bdb503a909fc040"),
                           URLQueryItem(name: "tags", value: tagsString),
                           URLQueryItem(name: "per_page", value: "1"),
                           URLQueryItem(name: "format", value: "json"),
                           URLQueryItem(name: "nojsoncallback", value: "1"),
                           URLQueryItem(name: "lang", value: "ru"),
                           URLQueryItem(name: "lon", value: longitude),
                           URLQueryItem(name: "lat", value: latitude),]
        urlComponents.queryItems = queryItems
        guard let resultURL = urlComponents.url else { fatalError() }
        
        return resultURL
    }
    
    static func parseRequestBackground(data: Data) -> NSString? {
        var urlPhoto: NSString?
        
        do{
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            guard let photos = jsonResult?[kPhotos] as? [String:Any] else { fatalError() }
            guard let photo = photos[kPhoto] as? [Any] else { fatalError() }
            guard let dictionary = photo[0] as? [String:Any] else { fatalError() }
            guard let photoId = dictionary[kId] as? String else { fatalError() }
            guard let server = dictionary[kServer] as? String else { fatalError() }
            guard let farm = dictionary[kFarm] as? Int else { fatalError() }
            guard let secret = dictionary[kSecret] as? String else { fatalError() }
            urlPhoto = "https://farm\(farm).staticflickr.com/\(server)/\(photoId)_\(secret).jpg" as NSString
        }
        catch {
            print("error")
        }
        return urlPhoto
    }
    
    static func loadImage(urlString: NSString, completion: @escaping (UIImage) -> Void) {
        guard let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { fatalError() }
        guard let searchUrl = NSURL(string: url as String) else { fatalError() }
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: searchUrl as URL) { (data, response, error) in
            let image = UIImage.init(data: data!)
                completion(image!)
        }
        task.resume()
    }
}
