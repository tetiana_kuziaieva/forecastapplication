//
//  TKLocationManager.swift
//  TKWeatherApplication
//
//  Created by Katherine on 04.05.17.
//  Copyright © 2017 student. All rights reserved.
//

import Foundation

struct LocationParameters {
    let latitude: Double
    let longitude: Double
}

class LocationManager {
    static func getLocation() -> LocationParameters {
        let coordinate = LocationParameters(latitude: 49.988358, longitude: 36.232845)
        return coordinate
    }
}
