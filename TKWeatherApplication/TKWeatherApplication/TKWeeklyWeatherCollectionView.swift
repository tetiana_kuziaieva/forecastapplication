//
//  TKWeeklyWeatherCollectionView.swift
//  TKWeatherApplication
//
//  Created by student on 5/3/17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit

class TKWeeklyWeather: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 24
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCollectionViewCell, for: indexPath) as? TKForecastCollectionViewCell else { fatalError() }
        
        return cell
    }
}
